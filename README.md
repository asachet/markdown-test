# markdown-test

A code block can contain a `#`:

```
# Important comment
important_code(important_arg)
```

A code block can be nested in a list

1. A first item

    ```
    amazing_result = cool_code()
    ```
	
2. A second item

But a code block cannot contain a `#` while nested in a list?!

1. First item

    ```
    # broken comment
    working_code()
    ```

2. Last item

This is a bug!